
<head>
	<title><?php
	if($pageTitle != "") 
		echo $pageTitle; 
	echo $websiteTitle;
	?></title>
	<link rel="stylesheet" media="all" href="<?php echo $backDir; ?>styles/style.php" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<link href="http://fonts.googleapis.com/css?family=Raleway:500,300,900" rel="stylesheet" type="text/css">
	<script>
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});
	</script>
</head>
<body>