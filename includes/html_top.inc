	<div class="nav2 nav-about">
		<div id="half"></div>
		<div class="arrow"></div>
		<div id="wrap">
		<ul>
			<li><a href="#company-profile">Company Profile</a></li>
			<li><a href="#vision-mission-statement">Vision-Mission Statement</a></li>
			<li><a href="#seven-core-principles">Seven Core Principles</a></li>
		</ul>
		</div>
	</div>
	<div class="nav2 nav-services">
		<div id="half"></div>
		<div class="arrow"></div>
		<div id="wrap">
		<ul>
			<li><a href="#motor-rewinding">Motor Rewinding</a></li>
			<li><a href="#transformers">Transformers</a></li>
			<li><a href="#generators">Generators</a></li>
			<li><a href="#testing-equipments">Testing Equipments</a></li>
			<li><a href="#other-services">Other Services</a></li>
		</ul>
		</div>
	</div>
	<div class="nav2 nav-clients">
		<div id="half"></div>
		<div class="arrow"></div>
		<div id="wrap">
		<ul>
			<li><a href="#our-clients">Our Clients</a></li>
			<li><a href="#clients-testimony">Clients Testimony</a></li>
		</ul>
		</div>
	</div>
	<div class="nav2 nav-contact">
		<div id="half"></div>
		<div class="arrow"></div>
		<div id="wrap">
		<ul>
			<li><a href="#contact-us">Contact Us</a></li>
			<li><a href="#social-media">Social Media</a></li>
			<li><a href="#location">Location</a></li>
		</ul>
		</div>
	</div>
	<div id="top">
		<div id="half"></div>
		<div class="arrow"></div>
		<div id="top_base">
			<center>
			<ul>
				<li><a href="#about" id="top_about" class="non">About</a></li>
				<li><a href="#services" id="top_services" class="non">Services</a></li>
			</ul>
			<a href="#home" class="logo"></a>
			<ul>
				<li><a href="#clients" id="top_clients" class="non">Clients</a></li>
				<li><a href="#contact" id="top_contact" class="non">Contact</a></li>
			</ul>
			</center>
		</div>
	</div>