
	<div id="footer">
		<div id="half"></div>
		<div id="wrap">
			<span>
				<h3>About</h3>
				<ul>
					<li><a href="#company-profile">Company Profile</a></li>
					<li><a href="#vision-mission-statement">Vision-Mission Statement</a></li>
					<li><a href="#seven-core-principles">Seven Core Principles</a></li>
				</ul>
			</span>
			<span>
				<h3>Services</h3>
				<ul>
					<li><a href="#motor-rewinding">Motor Rewinding</a></li>
					<li><a href="#transformers">Transformers</a></li>
					<li><a href="#generators">Generators</a></li>
					<li><a href="#testing-equipments">Testing Equipments</a></li>
				</ul>
			</span>
			<span>
				<h3>Client Testimony</h3>
				<ul>
					<li><div class="quote"><img src="images/skin/default/bg/quote.png">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, eos.</div><p>- Name<br>Position, Company</p><a href="#clients-testimony" class="sbutton">Read more...</a></li>
				</ul>
			</span>
		</div>
	</div>