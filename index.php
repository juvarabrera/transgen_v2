<?php 
	$backDir = '';
	require_once($backDir.'config.php');
	$pageTitle = 'Welcome to ';
?>
<?php include('includes/html_doctype.inc'); ?>
<?php include('includes/html_head.inc'); ?>
<div id="container">

	<!-- START HOME -->
	<div id="content_wrap" class="home-wrap">
		<div id="main">
			<a class="dir home" name="home"></a>
			<div id="half"></div>
			<div id="wrap">
				<h1>Customer satisfaction is our concern.</h1>
				<div id="photo" style="background-image: url(images/skin/default/bg/cover.jpg);"></div>
			</div>
		</div>
		<div id="content">
			<a class="dir" name=""></a>
			<div id="half"></div>
			<div id="center">
				<p>
				<table class="threecol">
					<tr>
						<td><span>Who are we?</span>Transgen Phils. Co. Inc. is a leading company within the generator, motor and transformer repair industry, with years of experience within this sector, we offer a wide range of services in rewinding, repairing and overhauling service to users of all brands of AC/DC and HV/LV electric motors and generators, pumps, transformers and alternators.</td>
						<td><span>What do we do?</span>Since then, the company has grown to employ a highly skilled workforce to serve most of Philippines leading companies. These include customers in construction, factory, hotel, lift, management, manufacturing, mining, steel, cement, shipping, sugar industries, private and semi - government sectors.</td>
						<td><span>Why us?</span>Our mission is to provide outstanding service and quality in workmanship to our customers at reasonable prices - regardless of the status of their company or the type of their industrial requirement.</td>
					</tr>
				</table>
				</p>
			</div>
			<div class="arrow"></div>
		</div>
	</div>
	<!-- END HOME -->



	<!-- START ABOUT -->
	<div id="content_wrap" class="about-wrap">
		<div id="main">
			<a class="dir about" name="about"></a>
			<div id="half"></div>
			<div id="wrap">
				<h1>About</h1>
				<div id="photo" style="background-image: url(images/skin/default/bg/cover.jpg);"></div>
			</div>
		</div>
		<div id="content">
			<a class="dir company-profile" name="company-profile"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Company Profile</td>
					<td></td>
				</tr></table></h2>
				<p>Transgen Phils. CO.INC. is a leading company within the motor repair industry, with years of experience within this sector, we offer a wide range of services in rewinding, repairing and overhauling service to users of all brands of AC/DC and HV/LV electric motors and generators, pumps, transformers and alternators.</p>

				<p>Since then, the company has grown to employ a highly skilled workforce to serve some of Philippines leading companies. These include customers in construction, factory, hotel, lift, management, manufacturing, private and semi - government sectors.</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir vision-mission-statement" name="vision-mission-statement"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Vision-Mission Statement</td>
					<td></td>
				</tr></table></h2>
				<p>
				<table class="twocol">
					<tr>
						<td><span>Our Vision</span>Transgen Phils. shall become the leader in the field of electrical repair and servicing, through constantly upgrading and infusing fresh and innovative ideas and technology while optimizing flexibility for greater functionality and efficiency, to deliver economically and environmentally sustainable products and services.</td>
						<td><span>Our Mission</span>Transgen Phils. operates to undertake the following purposes and objectives:
							<ul>
								<li>Transgen Phils. shall only offer the safest, environmentally sound, and economically friendly products and service.</li>
								<li>Transgen Phils. commits to produce only high quality works that meets international high standards.</li>
								<li>Transgen Phils. while pursuing business activities shall ensure to create more jobs and growth opportunities, through continuous learning programs, for its employees.</li>
							</ul>
						</td>
					</tr>
				</table>
				</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir seven-core-principles" name="seven-core-principles"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Seven Core Principles</td>
					<td><a href="#home" class="sbutton">Top</a></td>
				</tr></table></h2>
				<p>
				<table class="twocol">
					<tr>
						<td>TRANSGEN Phils. persistently drives to attain its company objectives through adopting and adhering to the following valued principles:
						<ol>
							<li>Pursuit of Integrity and professionalism</li>
							<li>Partnership with customers</li>
							<li>Quality and efficient delivery of service</li>
							<li>Deployment and continuously upgrading of technology and resources</li>
							<li>Adaptability to change and flexibility</li>
							<li>Sustaining operational profitability and marketability</li>
							<li>Underscoring Social Responsibility</li>
						</ol>
						</td>
						<td>
						</td>
					</tr>
				</table>
				</p>
			</div>
			<div class="arrow"></div>
		</div>
	</div>
	<!-- END ABOUT -->



	<!-- START SERVICES -->
	<div id="content_wrap" class="services-wrap">
		<div id="main">
			<a class="dir services" name="services"></a>
			<div id="half"></div>
			<div id="wrap">
				<h1>Services</h1>
				<div id="photo" style="background-image: url(images/skin/default/bg/cover.jpg);"></div>
			</div>
		</div>
		<div id="content">
			<a class="dir motor-rewinding" name="motor-rewinding"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Motor Rewinding</td>
					<td></td>
				</tr></table></h2>
				<p>Transgen Phils. Co. Inc.offers the ability to redesign and rebuild motors to your exact specifications. Our professional engineering can change a motors horsepower, speed, voltage, or frequency to meet your requirements. We can also provide the proper windings for frequency drive applications, protection against abrasives, and contamination or moisture laden atmospheres that destroy normal windings.</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir transformers" name="transformers"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Transformers</td>
					<td></td>
				</tr></table></h2>
				<p>Transgen Phils. Co. Inc.have the knowledge to rewind and repair all types of medium voltage, single and three phase control and power transformers including welding, step up and down variations with single or multi-tap connections. Dual and Auto wound, enclosed or open. 

				<br><br>We also rewind and repair clutch and brake coils what ever the variant.</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir generators" name="generators"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Generators</td>
					<td></td>
				</tr></table></h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, sint impedit suscipit quaerat repellendus aperiam officiis. Quidem enim eaque ratione saepe quod placeat debitis. Ratione, animi, nostrum dolores quas sint laudantium voluptatibus debitis. Tenetur, fugit, voluptatibus nulla quos quas possimus libero qui nostrum voluptates quis maxime provident beatae facere quibusdam?</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir testing-equipments" name="testing-equipments"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Testing Equipments</td>
					<td></td>
				</tr></table></h2>
				<p>Customers will surely benefit from these numerous standard acquisitions of testing equipments considered most advanced</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir other-services" name="other-services"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Other Services</td>
					<td><a href="#home" class="sbutton">Top</a></td>
				</tr></table></h2>
				<p>
				<table class="twocol">
					<tr>
						<td>TRANSGEN Phils. people have proven sophisticated and specialized skills and know-how in undertaking the following reliable services with efficiency:
						<ul>
							<li>Rewinding of Large AC/DC Motors, Generators and Transformers</li>
							<li>Repair/ Fabrication of Commutator assembly, carbon brush holder & slip- ring; MCC and Switch gear</li>
							<li>Preventive Maintenance of rotating Electrical Equipments & Substation, Low and Medium Voltage Switch gear/Panel boards, etc.</li>
							<li>Industrial Design and Installation</li>
							<li>Consultancy and Design Services</li>
							<li>Supply of Electrical/ Electronics and Mechanical Equipments and its parts</li>

						</ul></td>
						<td></td>
					</tr>
				</table>
				</p>
			</div>
			<div class="arrow"></div>
		</div>
	</div>
	<!-- END SERVICES -->



	<!-- START CLIENTS -->
	<div id="content_wrap" class="clients-wrap">
		<div id="main">
			<a class="dir clients" name="clients"></a>
			<div id="half"></div>
			<div id="wrap">
				<h1>Clients</h1>
				<div id="photo" style="background-image: url(images/skin/default/bg/cover.jpg);"></div>
			</div>
		</div>
		<div id="content">
			<a class="dir our-clients" name="our-clients"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Our Clients</td>
					<td></td>
				</tr></table></h2>
				<p>
				<table class="twocol">
					<tr>
						<td>Most of our major customers are from these industries:
						<ul>
							<li>Sugar Industry</li>
							<li>Steel Industry</li>
							<li>Mining Industry</li>
							<li>Cement Industry</li>
							<li>Shipping Industry</li>
							<li>Manufacturing Industry</li>
							<li>Power Generator Plants</li>
							<li>Commercial and Industrial Buildings</li>
						</ul></td>
						<td></td>
					</tr>
				</table>
				</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir clients-testimony" name="clients-testimony"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Clients Testimony</td>
					<td><a href="#home" class="sbutton">Top</a></td>
				</tr></table></h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, sint impedit suscipit quaerat repellendus aperiam officiis. Quidem enim eaque ratione saepe quod placeat debitis. Ratione, animi, nostrum dolores quas sint laudantium voluptatibus debitis. Tenetur, fugit, voluptatibus nulla quos quas possimus libero qui nostrum voluptates quis maxime provident beatae facere quibusdam?</p>
			</div>
			<div class="arrow"></div>
		</div>
	</div>
	<!-- END CLIENTS -->



	<!-- START CONTACT -->
	<div id="content_wrap" class="contact-wrap">
		<div id="main">
			<a class="dir contact" name="contact"></a>
			<div id="half"></div>
			<div id="wrap">
				<h1>Contact</h1>
				<div id="photo" style="background-image: url(images/skin/default/bg/cover.jpg);"></div>
			</div>
		</div>
		<div id="content">
			<a class="dir contact-us" name="contact-us"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Send a Message</td>
					<td></td>
				</tr></table></h2>
				<p>
				<form action="contact.php" method="post">
				<table class="input twocol">
					<tr>
						<td>Name*</td>
						<td>Email*</td>
					</tr>
					<tr>
						<td><input type="text" name="name" placeholder="Your Name"></td>
						<td><input type="text" name="name" placeholder="Your Email"></td>
					</tr>
					<tr>
						<td>Subject*</td>
						<td>Company/ Organization</td>
					</tr>
					<tr>
						<td><input type="text" name="name" placeholder="Subject"></td>
						<td><input type="text" name="name" placeholder="Company/ Organization (not required)"></td>
					</tr>
					<tr>
						<td colspan="2">Message</td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="message" placeholder="Your Message Here..."></textarea></td>
					</tr>
				</table>
				<center><input type="reset" value="Reset Fields"><input type="submit" value="Send Message"></center>
				</form>
				</p>
			</div>
			<div class="arrow"></div>
		</div>
		<!--
		<div id="content">
			<a class="dir social-media" name="social-media"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Social Media</td>
					<td></td>
				</tr></table></h2>
				<p>

				</p>
			</div>
			<div class="arrow"></div>
		</div>
		<div id="content">
			<a class="dir location" name="location"></a>
			<div id="half"></div>
			<div id="center">
				<h2><table><tr>
					<td>Location</td>
					<td><a href="#home" class="sbutton">Top</a></td>
				</tr></table></h2>
				<p>

				</p>
			</div>
			<div class="arrow"></div>
		</div>-->
	</div>
	<!-- END CONTACT -->



<?php include('includes/footer.inc'); ?>
<?php include('includes/copyright.inc'); ?>
</div>
<?php include('includes/html_top.inc'); ?>
<?php include('includes/html_end.inc'); ?>