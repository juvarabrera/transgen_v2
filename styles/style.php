<?php header("Content-type: text/css"); ?>
body {
	margin: 0px;
	padding: 0px;
	background: #e3e3e3;
	font-family: raleway, sans-serif;
}
#container {
	position: relative;
	top: 140px;
}
#top {
	position: fixed;
	top: 0px;
	left: 0px;
	background-color: #e3e3e3;
	width: 100%;
}
#top #top_base {
	position: relative;
	width: 980px;
	height: 100%;
	margin: 0px auto;
	box-sizing: border-box;
	padding: 10px;
}
#top #half {
	position: absolute;
	left: 50%;
	background: #d5d5d5;
	width: 50%;
	height: 100%;
}
#top .arrow {
	position: absolute;
	bottom: -20px;
	left: 0px;
	width: 100%;
	height: 20px;
	background-image: url(../images/skin/default/bg/arrowdown_e3e3e3.png);
	background-repeat: no-repeat;
	background-position: center top;
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
#top #top_base a.logo {
	background: url(../images/skin/default/logo/top.png) no-repeat center center;
	background-size: contain;
	width: 159px;
	height: 120px;
	display: block;
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
#top #top_base ul {
	margin: 0px;
	padding: 0px;
	list-style-type: none;
	position: absolute;
	top: 0px;
}
#top #top_base ul:first-child {
	left: 0px;
	text-shadow: -5px 5px 1px rgba(0,0,0,.05);
}
#top #top_base ul:last-child {
	right: 0px;
	text-shadow: 5px 5px 1px rgba(0,0,0,.05);
}
#top #top_base ul li {
	padding: 60px 0px;
	margin: 0px 60px;
	float: left;
	text-transform: uppercase;
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
#top #top_base ul li a {
	text-decoration: none;
	color: #555;
	padding: 2px 5px;
	font-weight: 500;
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
#top #top_base ul li a:hover {
	color: #7bc322;
	text-shadow: 0px 1px 3px rgba(0,0,0,.05);
	border-bottom: 2px solid #94d741;
}
#top #top_base ul li a.selected {
	color: #7bc322;
	text-shadow: 0px 1px 3px rgba(0,0,0,.05);
	border-bottom: 2px solid #94d741;
}


.nav2 {
	position: fixed;
	top: 140px;
	left: 0px;
	width: 100%;
	box-sizing: border-box;
	background: #cb3d76;
	-webkit-transition: all .3s ease-in-out;
	-moz-transition: all .3s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .3s ease-in-out;
}
.nav2 #half {
	position: absolute;
	left: 50%;
	top: 0px;
	width: 50%;
	height: 100%;
	background: #b2245d;
}
.nav2 .arrow {
	background: url(../images/skin/default/bg/arrowdown_cb3d76.png) no-repeat top center;
	width: 100%;
	height: 20px;
	position: absolute;
	bottom: -20px;
	left: 0px;
}
.nav2 #wrap {
	box-sizing: border-box;
	width: 980px;
	margin: 0px auto;
	padding: 15px 0px;
}
.nav2 ul {
	position: relative;
	padding: 0px;
	margin: 0px;
	list-style-type: none;
}
.nav2 ul li {
	display: inline-block;
	margin: 0px 10px;
}
.nav2 ul li a {
	text-decoration: none;
	color: #ddd;
	padding: 0px 10px;
	text-transform: uppercase;
	font-size: 12px;
	text-shadow: -5px 5px 1px rgba(0,0,0,.1);
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
.nav2 ul li a:hover {
	color: white;
}


.nav-about {
	top: 0px;
}
.nav-services {
	top: 0px;
}
.nav-clients {
	top: 0px;
}
.nav-contact {
	top: 0px;
}


#main {
	box-sizing: border-box;
	width: 100%;
	height: 400px;
	background: #111;
	position: relative;
}
#main #wrap {
	position: relative;
	width: 980px;
	box-sizing: border-box;
	height: 100%;
	margin: 0px auto;
}
#main h1 {
	color: #e3e3e3;
	text-shadow: -5px 5px 3px black;
	position: absolute;
	width: 400px;
	line-height: 200%;
	top: 40%;
	left: 20px;
	font-weight: 500;
}
#main #half {
	background: #0a0a0a;
	width: 50%;
	height: 100%;
	position: absolute;
	left: 50%;
	top: 0px;
}
#main .arrow {
	background: url(../images/skin/default/bg/arrowdown_111111.png) no-repeat center top;
	width: 100%;
	height: 20px;
	position: absolute;
	left: 0px;
	top: 0px;
}
#photo {
	background-repeat: no-repeat;
	background-position: right center;
	width: 462px;
	height: 100%;
	position: absolute;
	top: 0px;
	right: 0px;
}

#content_wrap #content {
	min-height: 50px;
	position: relative;
	overflow: hidden;
}
#content_wrap #content #half {
	position: absolute;
	left: 50%;
	top: 0px;
	width: 50%;
	height: 100%;
}
#content_wrap #content .arrow {
	position: absolute;
	top: 0px;
	left: 0px;
	width: 100%;
	height: 20px;
	background-repeat: no-repeat;
	background-position: center top;
}
#content_wrap #content #center {
	width: 980px;
	margin: 100px auto;
	display: block;
	float: auto;
	position: relative;
}
#content_wrap #content:nth-child(even) {
	background-color: #94d741;
}
#content_wrap #content:nth-child(even) #half {
	background-color: #84cd2b;
}
#content_wrap #content:nth-child(even) .arrow {
	background-image: url(../images/skin/default/bg/arrowdown_f8f8f8.png);
}
#content_wrap #content:nth-child(2) .arrow {
	background-image: url(../images/skin/default/bg/arrowdown_111111.png);
}

#content_wrap #content:nth-child(odd) {
	background-color: #f8f8f8;
}
#content_wrap #content:nth-child(odd) #half {
	background-color: #ececec;
}
#content_wrap #content:nth-child(odd) .arrow {
	background-image: url(../images/skin/default/bg/arrowdown_94d741.png);
}
#content h1, #content h2, #content h3 {
	text-shadow: -5px 5px 3px rgba(0,0,0,.1);
	font-weight: 500;
}
#content h1 table, #content h2 table, #content h3 table {
	width: 100%;
}
#content h1 table tr td:last-child, #content h2 table tr td:last-child, #content h3 table tr td:last-child {
	text-align: right;
}
#content h1 table tr td:last-child a, #content h2 table tr td:last-child a, #content h3 table tr td:last-child a {
	position: relative;
	top: -10px;
}
#content p {
	text-shadow: -5px 5px 3px rgba(0,0,0,.15);
	line-height: 200%;
	font-weight: 300;
	font-size: 14px;
	overflow: hidden;
}
#footer {
	background: #111;
	width: 100%;
	position: relative;
}
#footer #half {
	position: absolute;
	left: 50%;
	top: 0px;
	width: 50%;
	height: 100%;
	background: #0a0a0a;
}
#footer #wrap {
	width: 980px;
	box-sizing: border-box;
	overflow: hidden;
	margin: 0px auto;
	padding: 50px 0px;
	position: relative;
}
#footer #wrap span {
	float: left;
	width: 34%;
	float: left;
	color: white;
}
#footer #wrap span:nth-child(odd) {
	width: 33%;
}
#footer #wrap span:not(:last-child) {
	text-shadow: -5px 5px 3px rgba(0,0,0,.8);
}
#footer #wrap span:last-child {
	text-shadow: 5px 5px 3px rgba(0,0,0,.8);	
}
#footer #wrap span h3 {
	margin-left: 5px;
	font-weight: 500;
	text-transform: uppercase;
	font-size: 14px;
}
#footer #wrap span ul {
	list-style-type: none;
	margin-top: 30px;
	margin-left: 0px;
	padding-left: 25px;
}
#footer #wrap span ul li {
	font-size: 13px;
	font-weight: 300;
	margin-bottom: 25px;
	text-transform: uppercase;
}
#footer #wrap span ul li a:not(.sbutton) {
	text-decoration: none;
	color: white;
	padding-left: 0px;
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
#footer #wrap span ul li a:not(.sbutton):hover {
	color: #94d741;
	padding-left: 3px;
}
.quote {
	line-height: 40px;
	font-style: italic;
	text-transform: initial;
}
.quote img {
	margin-right: 20px;
}
#footer #wrap span ul li p {
	text-align: right;
	display: block;
}
#copyright {
	background: url(../images/skin/default/bg/footerglow.png) no-repeat center top;
	position: relative;
}
#copyright #base {
	width: 980px;
	margin: 0px auto;
	box-sizing: border-box;
	padding: 20px;
	text-align: center;
}
#copyright #base a.logo {
	background: url(../images/skin/default/logo/footer.png) no-repeat center center;
	width: 93px;
	height: 70px;
	display: block;
	margin: 10px auto;
}
#copyright #base p {
	font-size: 11px;
	color: #333;
	line-height: 200%;
	text-shadow: 0px 3px 2px rgba(0,0,0,.15);
}
#copyright a {
	text-decoration: none;
	color: inherit;
	font-weight: 500;
}
a.sbutton {
	text-decoration: none;
	text-transform: uppercase;
	text-shadow: 0px 0px 0px transparent;
	color: #ddd;
	padding: 10px 20px;
	font-size: 11px;
	border-box: box-sizing;
	border-radius: 100px;
	background: #b2245d;
	background: -moz-linear-gradient(top,  #b2245d 0%, #cb3d76 46%, #b2245d 50%, #b2245d 53%, #e46196 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b2245d), color-stop(46%,#cb3d76), color-stop(50%,#b2245d), color-stop(53%,#b2245d), color-stop(100%,#e46196));
	background: -webkit-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: -o-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: -ms-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: linear-gradient(to bottom,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b2245d', endColorstr='#e46196',GradientType=0 );
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
a.sbutton:hover {
	color: white;
	box-shadow: 0px 0px 10px rgba(203,61,118,1);
}
a.button {
	text-decoration: none;
	text-shadow: 0px 0px 0px transparent;
	color: #ddd;
	padding: 15px 35px;
	font-size: 14px;
	border-box: box-sizing;
	border-radius: 100px;
	background: #b2245d;
	background: -moz-linear-gradient(top,  #b2245d 0%, #cb3d76 46%, #b2245d 50%, #b2245d 53%, #e46196 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b2245d), color-stop(46%,#cb3d76), color-stop(50%,#b2245d), color-stop(53%,#b2245d), color-stop(100%,#e46196));
	background: -webkit-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: -o-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: -ms-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: linear-gradient(to bottom,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b2245d', endColorstr='#e46196',GradientType=0 );
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
a.button:hover {
	color: white;
	box-shadow: 0px 0px 10px rgba(203,61,118,1);
}
a.dir {
	position: absolute;
	top: -148px;
}
table.onecol, table.twocol, table.threecol {
	text-shadow: -5px 5px 3px rgba(0,0,0,.15);
	line-height: 200%;
	font-weight: 300;
	font-size: 14px;
	width: 100%;
	margin: 0px;
	padding: 0px;
	font-size: 14px;
}
table.twocol td {
	width: 50%;
	box-sizing: border-box;
	padding: 0px 20px;
	vertical-align: top;
}
table.threecol td {
	width: 33%;
	box-sizing: border-box;
	padding: 20px;
	vertical-align: top;
}
table.twocol td span, table.threecol td span {
	font-size: 22px;
	font-weight: 500;
	display: block;
	margin-bottom: 20px;
}
table.input {
	font-size: 14px;
}
input[type="text"] {
	padding: 8px;
	margin-top: 10px;
	margin-bottom: 30px;
	border: 2px solid #bbb;
	width: 100%;
	box-sizing: border-box;
}
textarea {
	padding: 8px;
	margin-top: 10px;
	margin-bottom: 30px;
	border: 2px solid #bbb;
	width: 100%;
	box-sizing: border-box;
	resize: none;
	height: 200px;
}
input, textarea {
	font-family: raleway, sans-serif;
}
input:focus, textarea:focus {
	border: 2px solid #cb3d76;
	outline: none;
}

input[type="submit"], input[type="reset"] {
	text-decoration: none;
	text-shadow: 0px 0px 0px transparent;
	color: #ddd;
	border: 0px;
	margin: 0px 20px;
	padding: 15px 35px;
	font-size: 14px;
	border-box: box-sizing;
	cursor: pointer;
	border-radius: 100px;
	background: #b2245d;
	background: -moz-linear-gradient(top,  #b2245d 0%, #cb3d76 46%, #b2245d 50%, #b2245d 53%, #e46196 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b2245d), color-stop(46%,#cb3d76), color-stop(50%,#b2245d), color-stop(53%,#b2245d), color-stop(100%,#e46196));
	background: -webkit-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: -o-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: -ms-linear-gradient(top,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	background: linear-gradient(to bottom,  #b2245d 0%,#cb3d76 46%,#b2245d 50%,#b2245d 53%,#e46196 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b2245d', endColorstr='#e46196',GradientType=0 );
	-webkit-transition: all .15s ease-in-out;
	-moz-transition: all .15s ease-in-out;
	-ms-transition: all .15s ease-in-out;
	-o-transition: all .15s ease-in-out;
	transition: all .15s ease-in-out;
}
input[type="submit"]:hover, input[type="reset"]:hover {
	color: white;
	box-shadow: 0px 0px 10px rgba(203,61,118,1);
}